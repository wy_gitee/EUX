// #pragma once

#ifndef _H_EDITULTRA_
#define _H_EDITULTRA_

#include "framework.h"

extern char		**g_argv ;
extern int		g_argc ;
extern HINSTANCE	g_hAppInstance;
extern HWND		g_hwndMainWindow;
extern char		g_acAppName[100];

/*
extern HWND		g_hwndToolBar_FileMenu ;
extern HWND		g_hwndToolBar_EditMenu ;
extern HWND		g_hwndReBar ;
extern int		g_nReBarHeight ;
*/
extern HWND		g_hwndToolBar ;
extern int		g_nToolBarHeight ;

extern pcre		*pcreCppFunctionRe ;

extern char		g_acModuleFileName[ MAX_PATH ] ;
extern char		g_acModulePathName[ MAX_PATH ] ;

extern int		g_nZoomReset ;

int OnCreateWindow( HWND hWnd , WPARAM wParam , LPARAM lParam );
void UpdateAllWindows( HWND hWnd );
void OnResizeWindow( HWND hWnd , int nWidth , int nHeight );

#define CONFIG_KEY_MATERIAL_EDITULTRA	"EDITUL"

#endif
