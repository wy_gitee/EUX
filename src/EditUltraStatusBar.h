#ifndef _H_EDITULTRA_STATUSBAR_
#define _H_EDITULTRA_STATUSBAR_

#include "framework.h"

extern HWND		g_hwndStatusBar ;
extern int		g_nStatusBarHeight ;

void SetProcessingInfo( const char *format , ... );
void CleanProcessingInfo();

void UpdateStatusBarProcessingInfo();
void UpdateStatusBarLocationInfo();
void UpdateStatusBarEolModeInfo();
void UpdateStatusBarEncodingInfo();
void UpdateStatusBarSelectionInfo();
void UpdateStatusBar( HWND hWnd );

#endif
